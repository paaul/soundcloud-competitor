import React from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import Albums from './containers/Albums'
import FavoriteTracks from './containers/FavoriteTracks'
import AdminPage from './containers/AdminPage'
import Login from './containers/Login'
import {connect} from 'react-redux'
import {loadAlbums} from "./actions/albumsActions";




class App extends React.Component {

    render() {
        return (

            <div>
                <header>
                    <Link to="/login"><p>Login</p></Link>
                    <Link to="/albums"><p>Albums</p></Link>
                    <Link to="/albums/id"><p>FavoriteTracks</p></Link>
                    <Link to="/adminpage"><p>Admin page</p></Link>
                </header>
                <Switch>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/albums/" component={Albums} />
                    <Route exact path="/albums/id" component={FavoriteTracks} />
                    <Route exact path="/adminpage" component={AdminPage} />
                </Switch>

            </div>
        )
    }

    componentDidMount() {
        if (this.props.albums.length === 0) {
            this.props.loadData();
        }
    }
}

const mapStateToProps = state => ({
       albums: state.albums
    });

    const mapDispatchToProps = dispatch => ({
        loadData: () => dispatch(loadAlbums())
    });

export default connect(mapStateToProps, mapDispatchToProps)(App)


//желаемая структура
/*
    store.js
App.js
index.js
actions/
    types.js (константи екшенов зберігаємо тут)
    playerActions.js
    {YOUR_NAME}Actions.js
components/ (маленькі і середні компоненти, кнопки, форми, лісти)
    AudioPlayer.js
    {COMPONENT_NAME}.js
containers/ (компоненти-контейнери(сторінки))
    AdminBox.js
    {CONTAINER_NAME}.js
reducers/
    index.js  (rootReducer)
    playerReducer.js
    {YOUR_NAME}Reducer.js
*/
