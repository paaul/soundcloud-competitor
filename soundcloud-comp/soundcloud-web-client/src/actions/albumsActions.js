import {ALBUMS_LOADED} from "./types";

export const loadAlbums = () => dispatch => {
    fetch('/api/albums')
        .then(res => res.json())
        .then(data => dispatch({type:ALBUMS_LOADED, payload: data}))
}
