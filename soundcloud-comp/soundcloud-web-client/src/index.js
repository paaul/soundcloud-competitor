// import React from 'react'
// import ReactDOM from 'react-dom'
// import App from './App'
// import {BrowserRouter as Router} from 'react-router-dom'
// import styles from './index.css'
// import thunk from 'redux-thunk'
// import { composeWithDevTools } from 'redux-devtools-extension';
//
//
// import {createStore, applyMiddleware} from 'redux'
// import reducers from './reducers/index'
// import {Provider} from 'react-redux'
//
// const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))
//
// ReactDOM.render(<Provider store={store}><Router><App /></Router></Provider>, document.getElementById('root'))

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {BrowserRouter, Route} from 'react-router-dom'
import App from './App';
import store from './store'

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Route path={'/'} component={App}/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
