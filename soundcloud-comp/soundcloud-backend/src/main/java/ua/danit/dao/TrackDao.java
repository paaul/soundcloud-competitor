package ua.danit.dao;

import org.springframework.data.repository.CrudRepository;
import ua.danit.model.Track;


public interface TrackDao extends CrudRepository<Track, Long>{

}
