package ua.danit.dao;

import org.springframework.data.repository.CrudRepository;
import ua.danit.model.User;

import java.util.List;

public interface UserDao extends CrudRepository<User, Long> {

    List<User> findByEmailAndName(String email, String name);

}
