package ua.danit.model;

import com.google.common.base.MoreObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRACKS")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TRACK_TITLE")
    private String trackName;

    @Column(name = "ALBUM_TITLE")
    private String albumName;

    public Long getId() {
        return id;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("trackName", trackName)
                .add("albumName", albumName)
                .toString();
    }

    public Track(String trackName, String albumName) {
        this.trackName = trackName;
        this.albumName = albumName;
    }
}
