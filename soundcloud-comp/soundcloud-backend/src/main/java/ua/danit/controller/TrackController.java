package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.model.Track;
import ua.danit.service.TrackService;

import java.util.List;

@RestController
@RequestMapping("/api/track")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @RequestMapping(path = "/api/get-all-tracks", method = RequestMethod.GET)       // 3.1 GET /api/tracks
    public ResponseEntity<List<Track>> getAllTracks() {
        return new ResponseEntity<>(trackService.getAllTracks(), HttpStatus.OK);
    }

    @RequestMapping(path = "/api/get-all-tracks/{id}", method = RequestMethod.GET)  // ??? 3.2 GET /api/tracks/{id}
    public ResponseEntity<List<Track>> getById() {
        return new ResponseEntity<>(trackService.getAllTracks(), HttpStatus.OK);
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)                    // 3.3 POST /api/tracks
    public ResponseEntity<Track> addTrack(@RequestBody Track track) {
        trackService.addTrack(track);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }







}

//        3.4 PUT /api/tracks/{id}
//        3.5 DELETE /api/tracks/{id}
//        3.6 POST /api/tracks/{id}/like
//        3.7 GET /api/tracks?liked=true
//        3.8 POST /api/tracks/{id}/song mp3 upload.