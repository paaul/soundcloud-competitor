package ua.danit.service;

import ua.danit.model.Album;

import java.util.List;

public interface AlbumService {

    List<Album> getAllAlbums();

    void addAlbum(Album album);

    Album getById(Long id);
}
