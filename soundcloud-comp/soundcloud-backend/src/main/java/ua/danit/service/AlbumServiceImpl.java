package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDao;
import ua.danit.model.Album;

import java.util.List;


@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumDao albumDao;


    @Override
    public List<Album> getAllAlbums() {
        return null;
    }

    @Override
    public void addAlbum(Album album) {

    }

    @Override
    public Album getById(Long id) {
        return null;
    }
}
