package ua.danit.service;

import ua.danit.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findByEmailAndName(String email, String name);

    Optional<User> findByToken(String token);

    User addUser(User user);

    List<User> getAllUsers();

}