package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.TrackDao;
import ua.danit.model.Track;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackDao trackDao;

    @Override
    public List<Track> getAllTracks() {
        return (List<Track>) trackDao.findAll(); // class cast iterable list
    }

    @Override
    public void addTrack(Track track) {

    }

    @Override
    public Track getById(Long id) {
        return null;
    }
}


